import React, {Component} from 'react'
import style from "./style.css"
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import img from '../../img/close_red.svg'
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';

const items = [];

for (let i = 0; i < 10; i++ ) {
    items.push(<MenuItem value={i} key={i} primaryText={i} />);
}

class Modal extends Component {
    state = {
        value: null,
        valuenum:10
    };
    handleChange = (event, index, value) => this.setState({value});
    handleChangeNum = (event, index, valuenum) => {
        this.setState({valuenum});
    };
    deleteForm = (key) => {

    };
    render() {

        return (
            <div className="form">
                <div className="container">
                <SelectField className="select_text"
                    value={this.state.value}
                    onChange={this.handleChange}
                >
                    <MenuItem  primaryText="a" />
                    <MenuItem  primaryText="b" />
                    <MenuItem  primaryText="b" />
                    <MenuItem  primaryText="c" />
                </SelectField>

                <TextField className="select_number"
                             type="number"
                    value={this.state.valuenum}
                    onChange={this.handleChangeNum}
                           min="0"
                />


                <IconButton  className="delete_but" onClick={this.deleteForm()}>
                    <img className="close_img" src={img} alt=""/>
                </IconButton>
                </div>

            </div>
        )
    }
}


export default Modal