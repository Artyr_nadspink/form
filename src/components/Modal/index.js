import React, {Component} from 'react'
import style from "./style.css"
import IconButton from 'material-ui/IconButton';
import img from '../../img/close.svg'
class Modal extends Component {
    state = {
        open: true,
        entry: [1]
    };
    addForm = () => {
        this.setState({entry: [...this.state.entry, 1]})
    }
    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    render() {
        const actions = [
            <FlatButton
                className="save_button"
                label="Сохранить"
                primary={true}
                onClick={this.handleClose}
            />,
            <FlatButton
                className="cancel_button"
                label="Отмена"
                primary={false}
                keyboardFocused={true}
                onClick={this.handleClose}
            />,
        ];

        return (

            <div className="root">
                <MuiThemeProvider>
                    <Dialog
                        autoDetectWindowHeight={true}
                        actions={actions}
                        modal={true}
                        open={this.state.open}
                        onRequestClose={this.handleClose}>
                        <div className="header">Структура номеров <IconButton onClick={this.handleClose}>
                            <img src={img} alt=""/>
                        </IconButton></div>

                        {
                            this.state.entry.map((item, i) => {
                                return <Form key={i}/>
                            })
                        }
                        <FlatButton onClick={this.addForm} className="add_button" label="Добавить" primary={true}/>
                    </Dialog>

                    <RaisedButton label="Dialog" onClick={this.handleOpen} />
                </MuiThemeProvider>
            </div>
        );
    }
}


export default Modal